---
Metadata
Title: Αναβάθμιση του εξομοιωτή τερματικού με το fancygit.
Alternative Title: Upgrade the terminal emulator with fancygit.
Contributor: @nikaskonstantinos
Language: el
Date Issued: 18-01-2023
Publisher: Linux User Forum – Το forum των Ελλήνων χρηστών Linux – https://linux-user.gr/
Description: Αναβάθμιση του εξομοιωτή τερματικού  και των διεργασιών του  git με το fancygit από το αποθετήριο https://github.com/diogocavilha/fancy-git   του χρήστη https://github.com/diogocavilha. 
Category: Άρθρα και οδηγοί
Tags:  terminal, prompt, git.

---

![Fancygit prompt](https://gitlab.com/hdhnl/how-to-install-fancygit/-/raw/main/2023-01-18_10-49fancygit.png)
Εικόνα 1. Fancygit.

### Εισαγωγή.
 Αναβαθμίστε την εμφάνιση του γραφικού περιβάλλοντος της γραμμής εντολών σας με τη χρήση της εφαρμογής fancy-git.
 Δημιουργός είναι ο λάτρης του Linux, [Diogo Cavilha](https://github.com/diogocavilha) από την Βραζιλία.
  Η εφαρμογή  fancygit πρωταρχικό σκοπό έχει την αναβάθμιση της παρουσίασης των διεργασιών του git αλλά και εικονικών περιβάλλοντων για παράδειγμα της python, με κατανοητό τρόπο.
  
 
 ### Προϋποθέσεις.
 Για τη σωστή εμφάνιση των εικονιδίων του fancygit, χρειάζεται η επιλογή μίας από τις παρακάτω γραμματοσειρές στον εξομειωτή του τερματικού μας.
 - Sauce-Code-Pro-Nerd-Font-Complete-Windows-Compatible.ttf.
- DejaVu-Sans-Mono-Nerd-Font-Complete.ttf.
- DejaVu-Sans-Mono-Nerd-Font-Complete-Mono.ttf.
- JetBrains-Mono-Regular-Nerd-Font-Complete-Mono.ttf.
- JetBrains-Mono-Medium-Nerd-Font-Complete-Mono.ttf.

Για την εφαρμογή `` Termux:Emulator`` του Android η επιλογή γραμματοσειράς μπορεί να γίνει με την εγκατάσταση της εφαρμογής ``Termux:Styling`` μέσα από το ``F-droid``.
 
 ### Εγκατάσταση.
 ```
 curl -sS https://raw.githubusercontent.com/diogocavilha/fancy-git/master/install.sh | sh
 ```
 ### Χρήση.
1. Κάνουμε επανεκκίνηση του εξομοιωτή του τερματικού.
2. Επιλέγουμε το θέμα της εμφάνισης, από τη διεύθυνση του αποθετηρίου: <https://github.com/diogocavilha/fancy-git>, για παράδειγμα:
```
fancygit --color-scheme-batman
```
3. Καθώς και όποιαδήποτε μορφοποίηση ή συντόμευση εντολής επιθυμούμε από την ίδια ηλεκτρονική διεύθυνση.

**Καλή επιτυχία!!!** 
* * *

### Απεγκατάσταση.
```
curl -sS https://raw.githubusercontent.com/diogocavilha/fancy-git/master/uninstall.sh | sh
```


### Πηγές.
1. <https://github.com/diogocavilha/fancy-git>
2. <https://github.com/diogocavilha>
 
 

